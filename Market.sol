pragma solidity >=0.7.0 <0.9.0;

import "hardhat/console.sol";
/**
 * @title Owner
 * @dev Set & change owner
 */
contract Tika {



   struct Nft {
       bool sell;
       uint price;
       uint tokenId;
       address payable contractAddres;
       bytes32[] tags; 
       address owner;
       uint256 nftId;
       // iki tarafli onay icin
       bool sellerAccept;
       bool buyed;
       address buyer;

   }
   uint256 nftCount;
   string[] categories;
   
    mapping(string=>Nft[]) nftList;
    mapping(uint256=>Nft) nftItems;
   function addToMarket(bool  isSelling, uint  _price, address  payable _contractAddres, uint256 _tokenId, bytes32[] memory _tags) external returns (bool ok) {
       nftCount++;
       Nft memory nft_ = Nft(isSelling,_price,_tokenId,_contractAddres,_tags, msg.sender,nftCount,false,false, address(0));
       nftItems[nftCount] = nft_;

       for(uint8 i=0;i<_tags.length; i++) {
           string memory tagName = string(abi.encodePacked(_tags[i])); 
           nftList[tagName].push(nft_);
       }
   }
   function getTags(string memory _tag) external view returns (Nft[] memory) {
       return nftList[_tag];
   }

   function sell(uint256 nftId, address to , address from ) external returns (bool success) {
       nftItems[nftId].sell = false;
       //Nft storage nft_ = nftItems[nftId];
       //nft_.contractAddres.transfer(msg.sender, to, nft_.tokenId);
       return true;
   }
   function getCategories() external view returns(string[] memory) {
       return categories;
   }
   function buy(uint256 nftId, address to , address from ) external returns (bool success) {
       //nftItems[nftId].sell = false;
       //Nft storage nft_ = nftItems[nftId];
       //nft_.contractAddres.transfer(msg.sender, to, nft_.tokenId);
       //return true;
   }

    constructor() {
        categories.push("Art");
        categories.push("Game");
        categories.push("3D");
        categories.push("Horror");
        categories.push("Cartoon");
     }
   

}