// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "hardhat/console.sol";


contract OYK {

    struct Phase {
        uint256 phaseTime;
        uint8 percentage;
    }
    struct UserPhase {
        uint256 amount;
        address userAddress;
        uint256 phaseTime;
        bool phased;
    }


    uint16 attenderCount; 
    mapping(address=>uint256) attenderAmaount;
    mapping(address=>uint256) balances;
    mapping(address=>uint256) lockedBalances;
    Phase[] phases;
    address owner;
    mapping(address=>UserPhase[]) userPhases;
    address[] users;
    uint256 firstRelease;
    string _name;
    string _symbol;
    uint8 _decimal;
    uint256 _total;
    

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    modifier isOwner() {
        require(msg.sender==owner, "only owner can call this function");
        _;
    }

    function newBalance(uint256 total_balance, uint8 percent) external pure returns (uint256 newbalance) {
      return total_balance  * (percent/100);
    }


    function name() external view returns (string memory) {
        return _name;
    }
    function symbol() external view returns (string memory) {
        return _symbol;
    }
    function decimals() external view returns (uint8) {
        return _decimal;
    }
    function totalSupply() external view returns (uint256) {
        return _total;
    }

    function balanceOf(address accountAddress) public view returns (uint256 balance){
    return balances[accountAddress];
 }

    function totalBalanceOf(address accountAddress) public view returns (uint256 balance){
    return balances[accountAddress] + lockedBalances[accountAddress];
 }




    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(address(0) != _to, "Please use a burn function");
        require(balances[msg.sender] >= _value, "Balance is low");
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        emit Transfer(msg.sender, _to, _value);
        
        return true;
    }

    function addUser(address _user, uint256 balance) external isOwner returns (bool success)  {
        balances[_user] = 0;
        lockedBalances[_user] = balance;
        for(uint8 i=0; i <3;i++) {
            Phase memory phase = phases[i];
            UserPhase memory phs = UserPhase(0,_user, phase.phaseTime,false);
            userPhases[_user].push(phs);   
        }
        users.push(_user);
        return true;
    }

    function release() public {
        require(msg.sender==owner, "only owner can call this function");

        for (uint8 i=0;i<3; i++) {
            Phase storage phase = phases[i];
            if (block.timestamp > phase.phaseTime) {
                uint8 percent = phase.percentage;
                for (uint256 uid=0;uid<attenderCount;uid++) {
                    address uAddress = users[uid];
                    
                     for (uint8 pid=0;pid<3; pid++) {
                        if (userPhases[uAddress][pid].phaseTime <= phase.phaseTime
                        && userPhases[uAddress][pid].phased == false
                        ) {
                            userPhases[uAddress][pid].phased = true;
                            userPhases[uAddress][pid].amount = this.newBalance(totalBalanceOf(uAddress), percent);
                            userPhases[uAddress][pid].phaseTime = block.timestamp;
                            balances[uAddress] += userPhases[uAddress][pid].amount;
                            lockedBalances[uAddress] -= userPhases[uAddress][pid].amount;
                            emit Transfer(address(0),uAddress,userPhases[uAddress][pid].amount);
                        }

                     }
                }
            }
        } 

    }
    function changeOwner(address _owner) external {
        require(msg.sender==owner, "only owner can call this function");
        owner = _owner;
    }
    constructor() {
        _name = "OYK 2023 KIS";
        _symbol = "o23K";
        _decimal = 3;
        _total =150 * 10**_decimal;
        firstRelease = block.timestamp;
        Phase memory phase1 = Phase(firstRelease,10); 

        Phase memory phase2 = Phase(firstRelease + (10*60) ,5); 
        Phase memory phase3 = Phase(firstRelease + (30*60) ,20); 
        phases.push(phase1);
        phases.push(phase2);
        phases.push(phase3);
        owner = msg.sender;
    }

}