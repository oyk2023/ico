// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "hardhat/console.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v4.8.1/contracts/token/ERC721/ERC721.sol";
/**
 * @title Owner
 * @dev Set & change owner
 */
contract DragonLance is ERC721 {


    uint256 tokenId;

    constructor(string memory name_, string memory symbol_) ERC721(name_,symbol_) public {}


    event NewCard(uint256 indexed token);



    function spawn(address player_) external {
        tokenId++;
        _mint(player_, tokenId);
        tokenURI(tokenId);
        emit NewCard(tokenId);
    }

     function _baseURI() internal view override returns (string memory) {
        return "http://example.com/";
    }

}
